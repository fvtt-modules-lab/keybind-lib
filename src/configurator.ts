export class KeybindConfigurator extends FormApplication {
  constructor() {
    super(null);
  }

  get element(): JQuery<HTMLElement> {
    return super.element as JQuery<HTMLElement>;
  }

  static get defaultOptions(): FormApplication.Options {
    return {
      ...super.defaultOptions,
      title: "Keybinds",
      id: "keybind-configurator",
      template: "modules/keybind-lib/templates/configurator.hbs",
      resizable: false,
      width: 600,
      closeOnSubmit: false,
      submitOnChange: false,
      submitOnClose: false,
    };
  }

  getData(): any {
    const data = {
      modules: [],
    };
    for (const [
      name,
      registeredBind,
    ] of globalThis.KeybindLib.registeredBinds.entries()) {
      const [moduleName, bindName] = name.split(".");
      let moduleData = data.modules.find((m) => m.name == moduleName);
      if (!moduleData) {
        moduleData = {
          name: moduleName,
          title: (game.modules.get(moduleName).data as any).title,
          binds: [],
        };
        data.modules.push(moduleData);
      }

      moduleData.binds.push({
        bindName,
        ...registeredBind.options,
        key: registeredBind.keyBind.toString(),
      });
    }

    return data;
  }

  resetSetting(fullName: string): string {
    const [moduleName, bindName] = fullName.split(".");
    const defaultSetting = game.settings.settings.get(fullName)
      .default as string;
    if (defaultSetting) {
      game.settings.set(moduleName, bindName, defaultSetting);
    }
    return defaultSetting;
  }

  activateListeners(html: JQuery): void {
    super.activateListeners(html);
    this.element
      .find("input")
      .on("keydown", (evt) =>
        globalThis.KeybindLib._handleConfigInput(evt, html)
      );

    this.element.find(`button[name="reset"]`).on("click", () => {
      const formData = this._getSubmitData();

      for (const fullName of Object.keys(formData)) {
        this.resetSetting(fullName);
      }
      this.render(true);
    });

    this.element.find(".fa-undo[data-bind]").on("click", (evt) => {
      const fullName = evt.target.dataset["bind"];
      if (!fullName) return;
      const val = this.resetSetting(fullName);
      this.element.find(`input[name="${fullName}"]`).val(val);
      globalThis.KeybindLib._resolveConflicts(html);
    });

    globalThis.KeybindLib._resolveConflicts(html);
  }

  async _updateObject(
    event: unknown,
    formData: { [fullName: string]: unknown }
  ): Promise<void> {
    for (const [fullName, value] of Object.entries(formData)) {
      const [moduleName, bindName] = fullName.split(".");
      game.settings.set(moduleName, bindName, value);
    }
    this.render(true);
  }
}
